#Select Region and provide keys these are variables placed in the variables stored in the Gitlab Pipeline
terraform {
  backend "http" {}
  required_providers {
    awscloud = {
      source = "hashicorp/aws"
      version = "3.18.0"
    }
  }
}
#Production VPC
module "Production_VPC" {
  source = "./modules/production_vpc"
  vpc_cidr = "10.50.0.0/16"
  vpc_id = module.Production_VPC.vpc_id
}
#Production VPC Subnet defaults based on the 10.10.0.0/16 cidr
module "Production_Subnets" {
  source = "./modules/production_vpc_subnets"
  vpc_id = module.Production_VPC.vpc_id
}
#Production VPC IGW for public communication
module "Production_IGW" {
  source = "./modules/production_igw"
  vpc_id = module.Production_VPC.vpc_id
}
//#Production VPC NAT gateway to the public subnet for egress communication
module "Production_NAT" {
  source = "./modules/production_nat"
  subnet_id = module.Production_Subnets.subnet_pub_1b
}
#Production VPC Communication routes
module "Production_Route_Tables" {
  source = "./modules/production_routing_tables"
  gateway_id = module.Production_IGW.igw_id
  vpc_id = module.Production_VPC.vpc_id
  def_route_table_id = module.Production_VPC.vpc_default_route_table
  nat_gateway_id = module.Production_NAT.NAT_gateway_ID
  pub_1a_subnet_id = module.Production_Subnets.subnet_pub_1a
  pub_1b_subnet_id = module.Production_Subnets.subnet_pub_1b
  pri_1b_subnet_id = module.Production_Subnets.subnet_pri_1b
  pri_1a_subnet_id = module.Production_Subnets.subnet_pri_1a
}
#Production VPC Security Groups
module "Production_Security_Group" {
  source = "./modules/production_security_group"
  vpc_id = module.Production_VPC.vpc_id
  default_vpc_sg = module.Production_VPC.vpc_default_sg
}
#Iam policies for EKS cluster and nodes
module "Production_IAM" {
  source = "./modules/production_iam"
}
// module "Production_Bastion" {
//   source = "./modules/production_bastion"
//   scanner_subnet_id = module.Production_Subnets.subnet_pub_1a
// }
module "Production_EKS_Cluster" {
  source = "./modules/production_eks_cluster"
  esk_cluster_arn = module.Production_IAM.eks_cluster_arn
  priv_1a = module.Production_Subnets.subnet_pri_1a
  priv_1b = module.Production_Subnets.subnet_pri_1b
}
#Testing Kubernetes using GitLab instead of Terraform Node
module "Production_EKS_Nodes" {
  source = "./modules/production_eks_nodes"
  cluster_name = module.Production_EKS_Cluster.eks_cluster
  eks_arn = module.Production_IAM.eks_node_arn
  subnet_ids = [module.Production_Subnets.subnet_pri_1a, module.Production_Subnets.subnet_pri_1b]
  //  source_sg_id = [module.Security_Group.bastion_sg_id]
}
