#--- EKS node group ---
resource "aws_eks_node_group" "Production_nodegroup" {
  cluster_name = var.cluster_name
  node_group_name = "Production_EKS_nodegroup_2021"
  #role permissions
  node_role_arn = var.eks_arn
  subnet_ids = var.subnet_ids
  #Testing env needs to be updated for prod
  instance_types = ["t2.medium"]
  #required for ssh ssh_key needs to match bastion key-pair, sg needs to match bastion_sg
  //    remote_access {
  //      source_security_group_ids = var.source_sg_id
  //      ec2_ssh_key = var.key_name
  //    }
  scaling_config {
    desired_size = 1
    max_size     = 5
    min_size     = 1
  }
  tags = var.node_tags
}
