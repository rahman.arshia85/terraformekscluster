

variable "cluster_name" {}
variable "eks_arn" {}
variable "subnet_ids" {}
#attached for ssh into bastion - these must match
variable "key_name" {
  default = "Gitlab-Recovery-07-28"
}
variable "node_tags" {
  description = "Tags to apply to resources created by EKS Terraform Module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
    Name = "Production_EKS_Nodes"
  }
}
//  variable "source_sg_id" {}
