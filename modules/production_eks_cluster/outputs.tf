

output "endpoint" {
  value = aws_eks_cluster.Production_EKS_cluster.endpoint
}
output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.Production_EKS_cluster.certificate_authority[0].data
}
output "eks_cluster" {
  value = aws_eks_cluster.Production_EKS_cluster.id
}
