#--- AWS EKS cluster provisioning ---
resource "aws_eks_cluster" "Production_EKS_cluster" {
  name     = "Production_EKS_cluster_2021"
  role_arn = var.esk_cluster_arn
  vpc_config {
    subnet_ids = [var.priv_1a, var.priv_1b]
  }
}
