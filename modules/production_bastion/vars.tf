variable "instance_type" {
  default = "t2.nano"
}
variable "ami" {
  #ami = saved bastion aws ami
  default = "ami-0a91cd140a1fc148a"
}
variable "key_name" {
  default = "PB_Bastion_Key"
}
variable "scanner_subnet_id" {}

variable "bastion_tags" {
  description = "Tags to apply to resources created by Scanner Bastion module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "scanner"
    Name = "Production_Bastion_2021"
  }
}
