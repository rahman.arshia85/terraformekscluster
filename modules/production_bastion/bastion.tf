resource "aws_instance" "Production_Scanner_Bastion" {
  ami = var.ami
  instance_type = var.instance_type
  subnet_id = var.scanner_subnet_id
  tags = var.bastion_tags
  key_name = var.key_name
  associate_public_ip_address = true
}
