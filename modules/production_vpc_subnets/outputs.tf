output "subnet_pub_1a" {
  value = aws_subnet.Production_Public_Subnet_1a.id
}

output "subnet_pub_1b" {
  value = aws_subnet.Production_Public_Subnet_1b.id
}

output "subnet_pri_1a" {
  value = aws_subnet.Production_Private_Subnet_1a.id
}

output "subnet_pri_1b" {
  value = aws_subnet.Production_Private_Subnet_1b.id
}

