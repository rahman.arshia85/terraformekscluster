variable "cluster-name" {
  default = "Production_EKS_cluster_2021"
  type    = string
}
variable "subnet_public_1a_cidr" {
  default = "10.50.20.0/24"
}
variable "subnet_public_1b_cidr" {
  default = "10.50.21.0/24"
}
variable "subnet_private_1a_cidr" {
  default = "10.50.30.0/24"
}
variable "subnet_private_1b_cidr" {
  default = "10.50.31.0/24"
}
variable "public_ip_map" {
  default = true
}
variable "vpc_id" {}
variable "vpc_tags" {
  description = "Tags to apply to resources created by VPC_Subnet module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
  }
}
