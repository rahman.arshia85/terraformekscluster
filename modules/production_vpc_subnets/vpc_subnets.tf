#--- Pull from aws resources ---
data "aws_availability_zones" "azs" {
  state = "available"
}
#--- Create Public and private subnets ---
resource "aws_subnet" "Production_Public_Subnet_1a" {
  availability_zone = data.aws_availability_zones.azs.names[0]
  cidr_block = var.subnet_public_1a_cidr
  vpc_id = var.vpc_id
  map_public_ip_on_launch = var.public_ip_map
  tags = tomap({
    "Name"= "Production_Public_Subnet_1a_2021",
    "kubernetes.io/cluster/${var.cluster-name}"= "shared",
    "kubernetes.io/role/elb"= 1
  })
}
resource "aws_subnet" "Production_Public_Subnet_1b" {
  availability_zone = data.aws_availability_zones.azs.names[1]
  cidr_block = var.subnet_public_1b_cidr
  map_public_ip_on_launch = var.public_ip_map
  vpc_id = var.vpc_id
  tags = tomap({
    "Name"= "Production_Public_Subnet_1b_2021",
    "kubernetes.io/cluster/${var.cluster-name}"= "shared",
    "kubernetes.io/role/elb"= 1
  })
}
resource "aws_subnet" "Production_Private_Subnet_1a" {
  availability_zone = data.aws_availability_zones.azs.names[0]
  cidr_block = var.subnet_private_1a_cidr
  vpc_id = var.vpc_id
  tags = tomap({
    "Name"= "Production_Private_Subnet_1a_2021",
    "kubernetes.io/cluster/${var.cluster-name}"= "shared",
    "kubernetes.io/role/internal-elb"= 1
  })
}
resource "aws_subnet" "Production_Private_Subnet_1b" {
  availability_zone = data.aws_availability_zones.azs.names[1]
  cidr_block = var.subnet_private_1b_cidr
  vpc_id = var.vpc_id
  tags = tomap({
    "Name"= "Production_Private_Subnet_1b_2021",
    "kubernetes.io/cluster/${var.cluster-name}"= "shared",
    "kubernetes.io/role/internal-elb"= 1
  })
}

