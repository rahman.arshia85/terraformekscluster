

#--- Create Elastic IP required for the NAT gateway setup ---
resource "aws_eip" "Production_NAT_Gateway_EIP" {
  vpc = true
  tags = var.nat_eip_tags
}
resource "aws_nat_gateway" "Production_NAT" {
  allocation_id = aws_eip.Production_NAT_Gateway_EIP.id
  subnet_id = var.subnet_id
  tags = var.nat_tags
}
