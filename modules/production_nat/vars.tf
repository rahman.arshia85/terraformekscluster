

variable "subnet_id" {}
variable "nat_tags" {
  description = "Tags to apply to resources created by NAT module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
    Name = "NAT_Production_Gateway_2021"
  }
}
variable "nat_eip_tags" {
  description = "Tags to apply to resources created by NAT module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
    Name = "NAT_Production_EIP_2021"
  }
}
