
variable "vpc_id" {}
variable "igw_tags" {
  description = "Tags to apply to resources created by IGW module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
    Name = "Production_VPC_IGW_2021"
  }
}
