
#--- Create internet gateway ---
resource "aws_internet_gateway" "Production_VPC_IGW" {
  vpc_id = var.vpc_id
  tags = var.igw_tags
}
