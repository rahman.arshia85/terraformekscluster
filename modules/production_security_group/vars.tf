
variable "default_vpc_sg" {}

variable "Node_tags" {
  description = "Tags to apply to resources created by Security_Group module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
    Name = "SG_Node_2021"
  }
}
variable "vpc_id" {}
