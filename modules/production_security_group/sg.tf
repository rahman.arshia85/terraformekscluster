#Node Security Group
resource "aws_security_group" "SG_Node" {
  name = "SG_Node_2021"
  vpc_id = var.vpc_id
  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  tags = var.Node_tags
}

//resource "aws_security_group_rule" "scanner_sg_rule" {
//  type = "ingress"
//  from_port = 0
//  to_port = 0
//  protocol = "-1"
//  cidr_blocks = ["20.0.0.0/16"]
//  security_group_id = var.default_vpc_sg
//}
