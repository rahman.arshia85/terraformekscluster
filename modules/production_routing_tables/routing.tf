#--- Routing Tables ---
#IGW is required for a NAT gateway creation
resource "aws_route_table" "production_public_route" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.gateway_id
  }
  tags = var.public_route_tags
}
#provided by the vpc module in the main file - Nat used for Egress communication
resource "aws_default_route_table" "production_private_route" {
  default_route_table_id = var.def_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = var.nat_gateway_id
  }
  tags = var.private_route_tags
}
#--- Subnet Associations ---
resource "aws_route_table_association" "production_pubic_1a" {
  subnet_id = var.pub_1a_subnet_id
  route_table_id = aws_route_table.production_public_route.id
}
resource "aws_route_table_association" "production_public_1b" {
  subnet_id = var.pub_1b_subnet_id
  route_table_id = aws_route_table.production_public_route.id
}
resource "aws_route_table_association" "production_private_1a" {
  subnet_id = var.pri_1a_subnet_id
  route_table_id = aws_default_route_table.production_private_route.id
}
resource "aws_route_table_association" "production_private_1b" {
  subnet_id = var.pri_1b_subnet_id
  route_table_id = aws_default_route_table.production_private_route.id
}



