
variable "vpc_id" {}
#Removed unnecessary igw for tighter security
variable "gateway_id" {}
variable "public_route_tags" {
  description = "Tags to apply to resources created by Routing_Tables module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
    Name = "Production_Public_Route_2021"
  }
}
variable "private_route_tags" {
  description = "Tags to apply to resources created by Routing_Tables module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
    Name = "Production_Private_Route_2021"
  }
}
variable "def_route_table_id" {}
variable "nat_gateway_id" {}
variable "pub_1a_subnet_id" {}
variable "pub_1b_subnet_id" {}
variable "pri_1a_subnet_id" {}
variable "pri_1b_subnet_id" {}
