output "vpc_id" {
  value = aws_vpc.Production_VPC.id
}
output "vpc_default_route_table" {
  value = aws_vpc.Production_VPC.default_route_table_id
}
output "vpc_default_sg" {
  value = aws_vpc.Production_VPC.default_security_group_id
}