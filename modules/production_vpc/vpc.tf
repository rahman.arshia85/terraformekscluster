resource "aws_vpc" "Production_VPC" {
  cidr_block = var.vpc_cidr
  tags = var.vpc_tags
}
