variable "vpc_cidr" {
  description = "CIDR block for VPC"
  type = string
  default = "10.50.0.0/16"
}
variable "vpc_name" {
  description = "VPC for production environment resources"
  type = string
  default = "Production_VPC_2021"
}
variable "vpc_tags" {
  description = "Tags to apply to resources created by VPC module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
    Name = "Production_VPC_2021"
  }
}
variable "vpc_id" {}
