

output "eks_cluster_iam" {
  value = aws_iam_role.Production_EKS_cluster_role.id
}
output "eks_cluster_arn" {
  value = aws_iam_role.Production_EKS_cluster_role.arn
}
output "eks_node_arn" {
  value = aws_iam_role.Production_EKS_node_role.arn
}
