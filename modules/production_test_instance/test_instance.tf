resource "aws_instance" "Production_Test_Instance" {
  ami = var.ami
  instance_type = var.instance_type
  subnet_id = var.test_subnet_id
  tags = var.test_instance_tags
  key_name = var.key_name
}
