variable "instance_type" {
  default = "t2.nano"
}
variable "ami" {
  #ami = ubuntu 18.04LTS
  default = "ami-0a91cd140a1fc148a"
}
variable "key_name" {
  default = "Gitlab-Recovery-07-28"
}
variable "test_subnet_id" {}

variable "test_instance_tags" {
  description = "Tags to apply to resources created by Test Instance module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
    Name = "Test_Instance_Production"
  }
}
